package com.layut.mig.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import com.sforce.soap.metadata.AsyncResult;
import com.sforce.soap.metadata.MetadataConnection;
import com.sforce.soap.metadata.PackageTypeMembers;
import com.sforce.soap.metadata.RetrieveRequest;
import com.sforce.soap.metadata.RetrieveResult;
import com.sforce.soap.metadata.StaticResource;
import com.sforce.soap.metadata.StaticResourceCacheControl;
import com.sforce.soap.metadata.UpdateMetadata;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectorConfig;

public class SFUtils {
	
	private static String sourceProfile = "Admin";
	private static String sourceLayout = "*";
	public RetrieveRequest retrieveRequest;
	public MetadataConnection metadataConnection;
	
	public String userName = "pankaj@gnt.pack";
	public String password = "test@1234";	
	//public static String userName = "packageorg1@reisystems.in";
	//public static String password = "test@12345";	
	public String strStaticResourceName = "ConfigPageLayouts";
	public String sourceObject = "*";	
	public static String packagePrefix = "GNT__";
	//public static String packagePrefix = "PKGORG1";
	
    public MetadataConnection setupConnection() throws Exception{
    	
		
		Properties prop = new Properties();
		InputStream input = null;
		input = new FileInputStream("config.properties");		
		prop.load(input);
		//String userName = prop.getProperty("username");
		//String password = prop.getProperty("password");
		
    	ConnectorConfig partnerConfig = new ConnectorConfig();
    	partnerConfig.setUsername(userName);
    	partnerConfig.setPassword(password);
    	
	    @SuppressWarnings("unused")
	    PartnerConnection partnerConnection = com.sforce.soap.partner.Connector.newConnection(partnerConfig);
	    
	    ConnectorConfig metadataConfig = new ConnectorConfig();	 
	    metadataConfig.setSessionId(partnerConnection.getSessionHeader().getSessionId());
	    metadataConnection = com.sforce.soap.metadata.Connector.newConnection(metadataConfig);
	    
	    return metadataConnection;
    }
    
	public RetrieveResult retrieveInfo() throws Exception {
		if(metadataConnection == null) {
			setupConnection();
		}
		retrieveRequest = new RetrieveRequest();
		retrieveRequest.setSinglePackage(true);
		com.sforce.soap.metadata.Package packageManifest = new com.sforce.soap.metadata.Package();
		ArrayList<PackageTypeMembers> types = new ArrayList<PackageTypeMembers>();
		
		PackageTypeMembers packageTypeMember = new PackageTypeMembers();			
		packageTypeMember.setName("Profile");
		packageTypeMember.setMembers(new String[] { sourceProfile });
		types.add(packageTypeMember);
		
		packageTypeMember = new PackageTypeMembers();
		packageTypeMember.setName("Layout");
		packageTypeMember.setMembers(new String[] { sourceLayout });
		types.add(packageTypeMember);
		
		packageTypeMember = new PackageTypeMembers();
		packageTypeMember.setName("CustomObject");
		packageTypeMember.setMembers(new String[] { sourceLayout });		
		types.add(packageTypeMember);
		
		packageManifest.setTypes((PackageTypeMembers[]) types.toArray(new PackageTypeMembers[] {}));			
		retrieveRequest.setUnpackaged(packageManifest);
		AsyncResult response = metadataConnection.retrieve(retrieveRequest);	
		while(!response.isDone()) {
		    Thread.sleep(1000);
		    response = metadataConnection.checkStatus(new String[] { response.getId()} )[0];
		}
		System.out.print("Retrieve succeded");
		RetrieveResult retrieveResult = metadataConnection.checkRetrieveStatus(response.getId());	
		return retrieveResult;			
	}
	
	public void deployInfo(byte[] zipBytes) throws Exception{
		
		StaticResource sr = new StaticResource();
		sr.setFullName("ConfigPageLayouts");
		sr.setCacheControl(StaticResourceCacheControl.Public);
		sr.setContentType("application/zip");
		sr.setContent(zipBytes);

		UpdateMetadata updateMetadata = new UpdateMetadata();
		updateMetadata.setCurrentName("ConfigPageLayouts");
		updateMetadata.setMetadata(sr);
		AsyncResult[] results = new AsyncResult []{};
		results = metadataConnection.update(new UpdateMetadata[]{updateMetadata});
		
		for(AsyncResult result : results ){
			while(!result.isDone()) {
				Thread.sleep(1000);
				result = metadataConnection.checkStatus(new String[] { result.getId()} )[0];
			}			
		}		
	}
}
