package com.layout.mig.xml.sobject;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.layut.mig.util.SFUtils;

public class ProcessSObject {
	private String sObjectXML;
	
	public ProcessSObject(String sObjectXML) {
		this.sObjectXML = sObjectXML;
	}
	
	//check if it is custom setting then return null else process
	public String process() throws SAXException, IOException, ParserConfigurationException, TransformerException {
		DocumentBuilderFactory inputFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder inputBuilder = inputFactory.newDocumentBuilder();
	    InputSource is = new InputSource(new StringReader(sObjectXML));
	    Document inputDoc = inputBuilder.parse(is);
		//optional, but recommended
		//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
	    //inputDoc.getDocumentElement().normalize();
	    
	    DocumentBuilderFactory outFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder outBuilder = outFactory.newDocumentBuilder();
		Document outDoc = outBuilder.newDocument();
		
		
		//System.out.println("Root element :" + inputDoc.getDocumentElement().getNodeName());
		if(isCustomSetting(inputDoc.getDocumentElement())) return null;
		// root elements
		Element rootElement = createRootElement(outDoc);
		processFieldsTag(inputDoc, rootElement, outDoc);
		processObjectTag(inputDoc, rootElement, outDoc);
		processNameFieldTag(inputDoc, rootElement, outDoc);
		
		return convertDomToString(outDoc);
	}
	
	private Boolean isCustomSetting(Element rootElement) {
		if(rootElement.getElementsByTagName("customSettingsType") != null && rootElement.getElementsByTagName("customSettingsType").item(0) != null 
					&& rootElement.getElementsByTagName("customSettingsType").item(0).getTextContent() != null) return true;
		return false;
	}
	
	private String convertDomToString(Document outDoc) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(outDoc);
		StreamResult result = new StreamResult(new StringWriter());

		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);

		transformer.transform(source, result);
		return result.getWriter().toString();
	}
	
	private Element createRootElement(Document outDoc) {
		Element rootElement = outDoc.createElement("CustomObject");
		Attr attr = outDoc.createAttribute("xmlns");
		attr.setValue("http://soap.sforce.com/2006/04/metadata");
		rootElement.setAttributeNode(attr);
		outDoc.appendChild(rootElement);
		return rootElement;
	}
	
	private void processFieldsTag(Document inputDoc, Element rootElement, Document outDoc) {
		NodeList nList = inputDoc.getElementsByTagName("fields");
		for (int temp = 0; temp < nList.getLength(); temp++) {
			if(isFormulaField(nList.item(temp))) continue;
			Element fields = outDoc.createElement("fields");
			rootElement.appendChild(fields);
			processFieldTag(nList.item(temp), fields, outDoc);
		}
	}
	
	private static final Set<String> OBJECT_TAGS_TO_PROCESS = new HashSet<String>();
	static {
		OBJECT_TAGS_TO_PROCESS.add("pluralLabel");
		OBJECT_TAGS_TO_PROCESS.add("label");
		OBJECT_TAGS_TO_PROCESS.add("allowInChatterGroups");
		OBJECT_TAGS_TO_PROCESS.add("deploymentStatus");
		OBJECT_TAGS_TO_PROCESS.add("description");
		OBJECT_TAGS_TO_PROCESS.add("enableActivities");
		OBJECT_TAGS_TO_PROCESS.add("enableBulkApi");
		OBJECT_TAGS_TO_PROCESS.add("enableSharing");
		OBJECT_TAGS_TO_PROCESS.add("enableStreamingApi");
		OBJECT_TAGS_TO_PROCESS.add("sharingModel");
		
	}
	private void processObjectTag(Document inputDoc, Element outRootElement, Document outDoc) {
		Element inputRootElement = inputDoc.getDocumentElement();
		//process label tag
		for(Node child = inputRootElement.getFirstChild(); child != null; child = child.getNextSibling()) {
	        if(child instanceof Element && OBJECT_TAGS_TO_PROCESS.contains(child.getNodeName())) {
	        	Element fieldVal = outDoc.createElement(child.getNodeName());
				String content = ((Element)child).getTextContent();//inputRootElement.getElementsByTagName(tagName).item(0).getTextContent();
				fieldVal.appendChild(outDoc.createTextNode(content));
				outRootElement.appendChild(fieldVal);
	        }
	    }
		/*for(String tagName : OBJECT_TAGS_TO_PROCESS) {
			if(inputRootElement.getElementsByTagName(tagName) == null || inputRootElement.getElementsByTagName(tagName).item(0) == null || inputRootElement.getElementsByTagName(tagName).item(0).getTextContent() == null) continue;
			
			
		}*/
	}
	
	private void processNameFieldTag(Document inputDoc, Element outRootElement, Document outDoc) {
		Element inputRootElement = inputDoc.getDocumentElement();
		//process label tag
		
		Element nameTag = outDoc.createElement("nameField");
		outRootElement.appendChild(nameTag);
		
		Element inputNameElement = ((Element)inputRootElement.getElementsByTagName("nameField").item(0));
		
		Element labelTag = outDoc.createElement("label");
		String labelContent = inputNameElement.getElementsByTagName("label").item(0).getTextContent();
		labelTag.appendChild(outDoc.createTextNode(labelContent));
		nameTag.appendChild(labelTag);
		
		Element typeTag = outDoc.createElement("type");
		String typeContent = inputNameElement.getElementsByTagName("type").item(0).getTextContent();
		typeTag.appendChild(outDoc.createTextNode(typeContent));
		nameTag.appendChild(typeTag);
		
		//display format is optional
		
		if(inputNameElement.getElementsByTagName("displayFormat") != null && inputNameElement.getElementsByTagName("displayFormat").item(0) != null && 
				inputNameElement.getElementsByTagName("displayFormat").item(0).getTextContent() != null) {
			Element dispFormatTag = outDoc.createElement("displayFormat");
			String dispFormContent = inputNameElement.getElementsByTagName("displayFormat").item(0).getTextContent();
			dispFormatTag.appendChild(outDoc.createTextNode(dispFormContent));
			nameTag.appendChild(dispFormatTag);
		}
		
	}
	
	private void processFieldTag(Node nNode, Element fieldElement, Document outDoc) {
		//System.out.println("\nCurrent Element :" + nNode.getNodeName());
		
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {
			
			Element eElement = (Element) nNode;
			
			//create entry for fullName
			processFullNameTag(eElement, fieldElement, outDoc);
			processReferenceToTag(eElement, fieldElement, outDoc);
			processOtherFieldTags(eElement, fieldElement, outDoc);
			//processValueSet(eElement, fieldElement, outDoc);			
			processPicklistTag(nNode, fieldElement, outDoc);
		}
	}
	
	private void processPicklistTag(Node nNode, Element outElement, Document outDoc) {		
		
		Element eElement = (Element) nNode;
		NodeList childNodes = eElement.getChildNodes();
		for(int nodeIndex=0; nodeIndex<childNodes.getLength(); nodeIndex++){
			Node childNode = childNodes.item(nodeIndex);		
			if(childNode.getNodeName() == "valueSet"){	
				Element fieldVal = outDoc.createElement(childNode.getNodeName());
				outElement.appendChild(fieldVal);
				processPicklistNode(childNode,fieldVal,outDoc);						
			}				
		}
	}
	
	private void processValueSetDefinition(Node nNode, Element fieldElement, Document outDoc){
		NodeList childNodes = nNode.getChildNodes();
		for(int nodeIndex=0; nodeIndex<childNodes.getLength(); nodeIndex++){
			Node childNode = childNodes.item(nodeIndex);	
			if(childNode.getNodeName() != "#text"){
				if(childNode.getNodeName() == "sorted"){
					Element fieldVal = outDoc.createElement(childNode.getNodeName());
					fieldVal.appendChild(outDoc.createTextNode(childNode.getTextContent()));
					fieldElement.appendChild(fieldVal);	
				}
				if(childNode.getNodeName() == "value"){
					Element fieldVal = outDoc.createElement(childNode.getNodeName());
					fieldElement.appendChild(fieldVal);	
					processValue(childNode,fieldVal,outDoc);				
				}				
			}
		}
	}
	private void processValueSettings(Node nNode, Element fieldElement, Document outDoc){
		NodeList childNodes = nNode.getChildNodes();
		for(int nodeIndex=0; nodeIndex<childNodes.getLength(); nodeIndex++){
			Node childNode = childNodes.item(nodeIndex);	
			if(childNode.getNodeName() != "#text"){	
				Element fieldVal = outDoc.createElement(childNode.getNodeName());
				fieldVal.appendChild(outDoc.createTextNode(childNode.getTextContent()));
				fieldElement.appendChild(fieldVal);
			}						
		}
	}
	private void processValue(Node nNode, Element fieldElement, Document outDoc){
		NodeList childNodes = nNode.getChildNodes();
		for(int nodeIndex=0; nodeIndex<childNodes.getLength(); nodeIndex++){
			Node childNode = childNodes.item(nodeIndex);
			if(childNode.getNodeName() != "#text"){					
				Element fieldVal = outDoc.createElement(childNode.getNodeName());
				fieldVal.appendChild(outDoc.createTextNode(childNode.getTextContent()));
				fieldElement.appendChild(fieldVal);	
			}							
		}
	}
	
	private void processPicklistNode(Node nNode, Element fieldElement, Document outDoc){
	
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {			
			NodeList childNodes = nNode.getChildNodes();
			for(int nodeIndex=0; nodeIndex<childNodes.getLength(); nodeIndex++){			
				Node childNode = childNodes.item(nodeIndex);
				if(childNode.getNodeName() != "#text"){
					Element fieldVal = outDoc.createElement(childNode.getNodeName());
					String content = childNode.getTextContent();
					if(childNode.getNodeName() == "controllingField" ){						
						if(content.contains("__c"))				
							fieldVal.appendChild(outDoc.createTextNode(addNamespace(content)));		
						else
							fieldVal.appendChild(outDoc.createTextNode(content));						
					}
					if(childNode.getNodeName() == "valueSettings")
						processValueSettings(childNode,fieldVal,outDoc);						
					
					if(childNode.getNodeName() == "valueSetDefinition")
						processValueSetDefinition(childNode,fieldVal,outDoc);						
					fieldElement.appendChild(fieldVal);	
				}							
			}				
		}		
	}
	
	private void processValueSet(Element eElement, Element outElement, Document outDoc) {
		if(eElement.getElementsByTagName("valueSet") == null || eElement.getElementsByTagName("valueSet").item(0) == null) return;
		//outElement.appendChild(eElement.getElementsByTagName("valueSet").item(0));
		Node imported = outDoc.importNode(eElement.getElementsByTagName("valueSet").item(0), true);
		outElement.appendChild(imported);
		/*Element fieldVal = outDoc.createElement("valueSet");
		outElement.appendChild(fieldVal);
		Element fieldValDf = outDoc.createElement("valueSetDefinition");
		fieldVal.appendChild(fieldValDf);
		
		Element ele = (Element)((Element)eElement.getElementsByTagName("valueSet").item(0)).getElementsByTagName("valueSetDefinition").item(0);
		for(Node child = ele.getFirstChild(); child != null; child = child.getNextSibling()) {
	        if(child instanceof Element && "value" == (child.getNodeName())) {
	        	Element fieldvalueDf = outDoc.createElement("value");
	    		fieldValDf.appendChild(fieldvalueDf);
	    		for(Node child1 = child.getFirstChild(); child1 != null; child1 = child1.getNextSibling()) {
	    	        if(child1 instanceof Element && "value" == (child.getNodeName())) {
	    	        	
	    	        }
	    		}
	        }
	    }
		
		
		Element fullNameDf = outDoc.createElement("fullName");
		fullNameDf.appendChild(outDoc.createTextNode("--"));
		fieldvalueDf.appendChild(fullNameDf);
		
		Element defaultVal = outDoc.createElement("default");
		defaultVal.appendChild(outDoc.createTextNode("false"));
		fieldvalueDf.appendChild(defaultVal);
		//String content = eElement.getElementsByTagName("fullName").item(0).getTextContent();
		//fieldVal.appendChild(outDoc.createTextNode(addNamespace(content)));*/
		
		
	}
	
	private Boolean isFormulaField(Node nNode) {
		Element eElement = (Element)nNode;
		if(eElement.getElementsByTagName("formula") != null && eElement.getElementsByTagName("formula").item(0) != null && 
				eElement.getElementsByTagName("formula").item(0).getTextContent() != null) return true;
		return false;
	}
	
	private void processFullNameTag(Element eElement, Element outElement, Document outDoc) {
		Element fieldVal = outDoc.createElement("fullName");
		String content = eElement.getElementsByTagName("fullName").item(0).getTextContent();
		fieldVal.appendChild(outDoc.createTextNode(addNamespace(content)));
		outElement.appendChild(fieldVal);
	}
	
	private void processReferenceToTag(Element eElement, Element outElement, Document outDoc) {
		if(eElement.getElementsByTagName("referenceTo") == null || eElement.getElementsByTagName("referenceTo").item(0) == null || 
					eElement.getElementsByTagName("referenceTo").item(0).getTextContent() == null) return;
		Element fieldVal = outDoc.createElement("referenceTo");
		String content = eElement.getElementsByTagName("referenceTo").item(0).getTextContent();
		if(content.contains("__c"))
			fieldVal.appendChild(outDoc.createTextNode(addNamespace(content)));
		else
			fieldVal.appendChild(outDoc.createTextNode(content));		
		outElement.appendChild(fieldVal);
	}
	
	private static final List<String> FIELD_TAGS_TO_PROCESS = new ArrayList<String>();
	static {
		FIELD_TAGS_TO_PROCESS.add("externalId");
		FIELD_TAGS_TO_PROCESS.add("label");
		FIELD_TAGS_TO_PROCESS.add("required");
		FIELD_TAGS_TO_PROCESS.add("scale");
		FIELD_TAGS_TO_PROCESS.add("inlineHelpText");
		FIELD_TAGS_TO_PROCESS.add("description");
		FIELD_TAGS_TO_PROCESS.add("type");
		FIELD_TAGS_TO_PROCESS.add("defaultValue");
		FIELD_TAGS_TO_PROCESS.add("trackTrending");
		FIELD_TAGS_TO_PROCESS.add("length");
		FIELD_TAGS_TO_PROCESS.add("unique");
		FIELD_TAGS_TO_PROCESS.add("caseSensitive");
		FIELD_TAGS_TO_PROCESS.add("precision");
		FIELD_TAGS_TO_PROCESS.add("visibleLines");
		FIELD_TAGS_TO_PROCESS.add("relationshipName");
		FIELD_TAGS_TO_PROCESS.add("summaryForeignKey");
		FIELD_TAGS_TO_PROCESS.add("relationshipLabel");
		FIELD_TAGS_TO_PROCESS.add("relationshipOrder");
		FIELD_TAGS_TO_PROCESS.add("reparentableMasterDetail");
		FIELD_TAGS_TO_PROCESS.add("summaryOperation");
		FIELD_TAGS_TO_PROCESS.add("masterLabel");
		
	}
	
	private void processOtherFieldTags(Element eElement, Element outElement, Document outDoc) {
		for(String tagName : FIELD_TAGS_TO_PROCESS) {
			if(eElement.getElementsByTagName(tagName) == null || eElement.getElementsByTagName(tagName).item(0) == null || eElement.getElementsByTagName(tagName).item(0).getTextContent() == null) continue;
			
			Element fieldVal = outDoc.createElement(tagName);
			String content = eElement.getElementsByTagName(tagName).item(0).getTextContent();
			if(tagName == "summaryForeignKey")
				content = processSummaryForeignKey(content);			
			fieldVal.appendChild(outDoc.createTextNode(content));
			outElement.appendChild(fieldVal);
		}
	}
	
	private String processSummaryForeignKey(String strContent){
		String strFieldVal = "";	
		String strSeparator = "";
		if(strContent.contains(".")){	
			strSeparator = ".";
			String strContents [] = strContent.split("\\"+strSeparator,2);
			for(int strIndex=0;strIndex<strContents.length;strIndex++){	     
				if(strContents[strIndex].contains("__c")){
					if(strIndex == 0)
						strFieldVal = strFieldVal + SFUtils.packagePrefix + strContents[strIndex];
					else
						strFieldVal = strFieldVal + strSeparator + SFUtils.packagePrefix + strContents[strIndex];
				}
				else
					strFieldVal = strContent;
			}
		}
		else
			strFieldVal = strContent;			
		return strFieldVal;
	}
	
	
	private String addNamespace(String str) {
		str = SFUtils.packagePrefix+ str;
		return str;
	}
	

}
