package com.layout.mig.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.layut.mig.util.SFUtils;
import com.sforce.soap.metadata.RetrieveResult;

public class ProcessProfiles {
	private RetrieveResult retrieveResult;
	private ZipOutputStream zipOutStream;
	public ProcessProfiles(RetrieveResult retrieveResult, ZipOutputStream zipOutStream) {
		this.retrieveResult = retrieveResult;
		this.zipOutStream = zipOutStream;
	}
	public void process() throws IOException, SAXException, ParserConfigurationException, TransformerException {
		byte[] zipBytes = retrieveResult.getZipFile();	
		ZipInputStream zipis = new ZipInputStream(new ByteArrayInputStream(zipBytes, 0, zipBytes.length));
		ZipEntry zipEntry = null;
		while((zipEntry = zipis.getNextEntry()) != null) {	
			if(zipEntry.getName().contains("profiles/")){
				System.out.println(zipEntry.getName());
				addEntryToZip(zipEntry.getName(), ProcessProfile(zipis));
			}
		}
	}

	private String ProcessProfile(ZipInputStream zipis) throws SAXException, IOException, ParserConfigurationException, TransformerException {
		String sObjectXMLStr = new String();
		Scanner sc = new Scanner(zipis);
		while (sc.hasNextLine()) {	            
			sObjectXMLStr += sc.nextLine();
		}
		//sc.close();
		return new ProcessAdminProfile(sObjectXMLStr).process();
	}
	private void addEntryToZip(String fileName, String content) throws IOException {
		ZipEntry e = new ZipEntry(fileName);
		zipOutStream.putNextEntry(e);       
		byte[] data = content.getBytes(); 
		zipOutStream.write(data, 0, data.length);
		zipOutStream.closeEntry();
	}

}
