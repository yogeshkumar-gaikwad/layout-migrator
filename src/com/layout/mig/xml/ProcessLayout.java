package com.layout.mig.xml;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.Text;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.layut.mig.util.SFUtils;

public class ProcessLayout {
	private String sObjectXML;

	public ProcessLayout(String sObjectXML) {
		this.sObjectXML = sObjectXML;
	}

	public String process() throws SAXException, IOException, ParserConfigurationException, TransformerException {
		DocumentBuilderFactory inputFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder inputBuilder = inputFactory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(sObjectXML));
		Document inputDoc = inputBuilder.parse(is);
		//optional, but recommended
		//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
		inputDoc.getDocumentElement().normalize();

		DocumentBuilderFactory outFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder outBuilder = outFactory.newDocumentBuilder();
		Document outDoc = outBuilder.newDocument();

		// root elements
		Element rootElement = createRootElement(outDoc);
		processLayoutSectionsTag(inputDoc, rootElement, outDoc);	
		processRelatedListTag(inputDoc, rootElement, outDoc);

		return convertDomToString(outDoc);
	}

	private void processLayoutSectionsTag(Document inputDoc, Element rootElement, Document outDoc) {
		NodeList layoutSectionsNodeList = inputDoc.getElementsByTagName("layoutSections");
		for (int temp = 0; temp < layoutSectionsNodeList.getLength(); temp++) {			
			Element layoutSections = outDoc.createElement("layoutSections");	
			rootElement.appendChild(layoutSections);
			processLayoutSectionsNode(layoutSectionsNodeList.item(temp), layoutSections, outDoc);			
		}
	}
	
	private void processLayoutSectionsNode(Node nNode, Element rootElement, Document outDoc) {
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {
			NodeList layoutSectionsChildNodes = nNode.getChildNodes();			
			for(int index=0; index<layoutSectionsChildNodes.getLength(); index++){				
				Node childNode = layoutSectionsChildNodes.item(index);
				if(childNode.getNodeName() != "#text"){
					if(childNode.getNodeName() == "layoutColumns"){
						Element layoutColumns = outDoc.createElement("layoutColumns");
						rootElement.appendChild(layoutColumns);
						processLayoutColumns(childNode,layoutColumns,outDoc);					
					}	
					else{
						Element fieldVal = outDoc.createElement(childNode.getNodeName());
						String content = ((Element)childNode).getTextContent().trim();//inputRootElement.getElementsByTagName(tagName).item(0).getTextContent();
						fieldVal.appendChild(outDoc.createTextNode(content));
						rootElement.appendChild(fieldVal);
					}					
				}					
			}		
		}
	}
	
	private void processLayoutColumns(Node layoutColumnNode, Element parentElement, Document outDoc){
		NodeList childNodes = layoutColumnNode.getChildNodes();		
		for(int index=0; index<childNodes.getLength();index++){
			Node layoutItemsNode = childNodes.item(index);
			if(layoutItemsNode.getNodeName() == "layoutItems"){
				Element layoutItems = outDoc.createElement("layoutItems");
				parentElement.appendChild(layoutItems);
				processLayoutItems(layoutItemsNode,layoutItems,outDoc);				
			}				
		}
	}
	
	private void processLayoutItems(Node layoutItemsNode, Element parentElement, Document outDoc){
		NodeList childNodes = layoutItemsNode.getChildNodes();
		for(int index=0; index<childNodes.getLength();index++){
			Node childNode = childNodes.item(index);
			String strFieldVal = "";
			if(childNode.getNodeName() != "#text" && childNode.getNodeName() != "customButtons")	{
				Element element = null;
				element = outDoc.createElement(childNode.getNodeName());				
				if(childNodes.item(index).getNodeName() == "field" && childNodes.item(index).getTextContent().contains("__c"))
					strFieldVal = addNamespace(childNode.getTextContent());	
				if(childNodes.item(index).getNodeName() == "page")
					strFieldVal = addNamespace(childNode.getTextContent());	
				if(strFieldVal.isEmpty())
					strFieldVal = childNode.getTextContent();
				element.appendChild(outDoc.createTextNode(strFieldVal));
				parentElement.appendChild(element);		
			}		
		}
	}
	
	private void processRelatedListTag(Document inputDoc, Element rootElement, Document outDoc){
		NodeList relatedListsNodeList = inputDoc.getElementsByTagName("relatedLists");
		for (int temp = 0; temp < relatedListsNodeList.getLength(); temp++) {
			Element relatedLists = outDoc.createElement("relatedLists");	
			rootElement.appendChild(relatedLists);
			processRelatedListsNode(relatedListsNodeList.item(temp), relatedLists, outDoc);			
		}
	}
	
	private void processRelatedListsNode(Node nNode, Element parentElement, Document outDoc) {
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {
			NodeList childNodes = nNode.getChildNodes();			
			for(int index=0; index<childNodes.getLength(); index++){				
				Node childNode = childNodes.item(index);
				String strFieldVal = "";
				if(childNode.getNodeName() != "#text" && childNode.getNodeName() != "customButtons"){
					Element element = null;
					element = outDoc.createElement(childNode.getNodeName());
					
					if(childNode.getNodeName() == "fields" || childNode.getNodeName() == "sortField" )						
						strFieldVal = processFields(childNode);						
					
					if(childNode.getNodeName() == "relatedList")
						strFieldVal = processRelatedList(childNode);
					
					if(childNode.getNodeName() == "sortOrder")
						strFieldVal = processSortOrder(childNode);
					
					element.appendChild(outDoc.createTextNode(strFieldVal));						
					parentElement.appendChild(element);					
				}
			}
		}	
	}
	
	private String processFields(Node fieldNode){
		String strFieldVal = "";
		if(fieldNode.getTextContent().contains("__c"))
			strFieldVal = addNamespace(fieldNode.getTextContent());
		if(strFieldVal.isEmpty())
			strFieldVal = fieldNode.getTextContent();
		return strFieldVal;
	}
	
	private String processSortOrder(Node fieldNode){
		return fieldNode.getTextContent();		
	}
	
	private String processRelatedList(Node relatedListNode){
		String strFieldVal = "";
		String strContent = relatedListNode.getTextContent();
		String strSeparator = "";
		if(strContent.contains(".") || strContent.contains("-")){			    	
			if(strContent.contains("."))
				strSeparator = ".";
			else
				strSeparator = "-";			    	 

			String strContents [] = strContent.split("\\"+strSeparator,2);
			for(int strIndex=0;strIndex<strContents.length;strIndex++){	     
				if(strContents[strIndex].contains("__c")){
					if(strIndex == 0)
						strFieldVal = strFieldVal + SFUtils.packagePrefix + strContents[strIndex];
					else
						strFieldVal = strFieldVal + strSeparator + SFUtils.packagePrefix + strContents[strIndex];
				}	
			}
		}
		else
			strFieldVal = strContent;
		//System.out.println("processRelatedList strFieldVal::"+strFieldVal);		
		return strFieldVal;
	}
	
	private String convertDomToString(Document outDoc) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(outDoc);
		StreamResult result = new StreamResult(new StringWriter());
		transformer.transform(source, result);
		return result.getWriter().toString();
	}

	private Element createRootElement(Document outDoc) {
		Element rootElement = outDoc.createElement("Layout");
		Attr attr = outDoc.createAttribute("xmlns");
		attr.setValue("http://soap.sforce.com/2006/04/metadata");
		rootElement.setAttributeNode(attr);
		outDoc.appendChild(rootElement);
		return rootElement;
	}	

	private String addNamespace(String str) {
		str = SFUtils.packagePrefix+ str;
		return str;
	}
}
