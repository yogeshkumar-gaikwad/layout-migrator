package com.layout.mig.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.layout.mig.xml.sobject.ProcessSObject;
import com.layut.mig.util.SFUtils;
import com.sforce.soap.metadata.RetrieveResult;

public class ProcessLayouts {
	private RetrieveResult retrieveResult;
	private ZipOutputStream zipOutStream;
	public ProcessLayouts(RetrieveResult retrieveResult, ZipOutputStream zipOutStream) {
		this.retrieveResult = retrieveResult;
		this.zipOutStream = zipOutStream;
	}
	public void process() throws IOException, SAXException, ParserConfigurationException, TransformerException {
		byte[] zipBytes = retrieveResult.getZipFile();	
		ZipInputStream zipis = new ZipInputStream(new ByteArrayInputStream(zipBytes, 0, zipBytes.length));
		ZipEntry zipEntry = null;
		while((zipEntry = zipis.getNextEntry()) != null) {	
			if(zipEntry.getName().contains("layouts/") && zipEntry.getName().contains("__c")){
				//System.out.println(zipEntry.getName());
				addEntryToZip("layouts/"+processLayoutName(zipEntry.getName()), ProcessLayout(zipis));
			}
		}
	}
	
	private String ProcessLayout(ZipInputStream zipis) throws SAXException, IOException, ParserConfigurationException, TransformerException {
		String sObjectXMLStr = new String();
		Scanner sc = new Scanner(zipis);
         while (sc.hasNextLine()) {	            
         	sObjectXMLStr += sc.nextLine();
         }
         //sc.close();
         return new ProcessLayout(sObjectXMLStr).process();
	}
	private void addEntryToZip(String fileName, String content) throws IOException {
		System.out.println("fileName::"+fileName);
		ZipEntry e = new ZipEntry(fileName);
		zipOutStream.putNextEntry(e);       
        byte[] data = content.getBytes(); 
        zipOutStream.write(data, 0, data.length);
        zipOutStream.closeEntry();
	}
	
	private String processLayoutName(String strLayoutName){
		
		String strFieldVal = "";
		
		String strContent = strLayoutName.substring(8,strLayoutName.length());
		System.out.println("before strContent::"+strContent);
		String strSeparator = "";
		if(strContent.contains("-")){	
			
			strSeparator = "-";			    	 

			String strContents [] = strContent.split("\\"+strSeparator,2);

			for(int strIndex=0;strIndex<strContents.length;strIndex++){	
				if(strIndex == 0)
					
					strFieldVal = strFieldVal + SFUtils.packagePrefix + strContents[strIndex];
				else
					strFieldVal = strFieldVal + strSeparator + SFUtils.packagePrefix + strContents[strIndex];						
			}
		}					
		return strFieldVal;
	}
	
}
