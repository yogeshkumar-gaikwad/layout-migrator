package com.layout.mig.xml;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.layut.mig.util.SFUtils;

public class ProcessAdminProfile {
	private String sObjectXML;

	public ProcessAdminProfile(String sObjectXML) {
		this.sObjectXML = sObjectXML;
	}

	public String process() throws SAXException, IOException, ParserConfigurationException, TransformerException {
		DocumentBuilderFactory inputFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder inputBuilder = inputFactory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(sObjectXML));
		Document inputDoc = inputBuilder.parse(is);
		//optional, but recommended
		//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
		inputDoc.getDocumentElement().normalize();

		DocumentBuilderFactory outFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder outBuilder = outFactory.newDocumentBuilder();
		Document outDoc = outBuilder.newDocument();

		// root elements
		Element rootElement = createRootElement(outDoc);
		processFieldPermissions(inputDoc, rootElement, outDoc);
		processLayoutAssignments(inputDoc, rootElement, outDoc);
		processRecordTypeVisibilities(inputDoc, rootElement, outDoc);
		return convertDomToString(outDoc);
	}
	
	private void processFieldPermissions(Document inputDoc, Element rootElement, Document outDoc){
		NodeList fieldPermissionsNodeList = inputDoc.getElementsByTagName("fieldPermissions");
		for (int temp = 0; temp < fieldPermissionsNodeList.getLength(); temp++) {
			Element fieldPermissions = outDoc.createElement("fieldPermissions");	
			rootElement.appendChild(fieldPermissions);
			processFieldPermissionsNode(fieldPermissionsNodeList.item(temp), fieldPermissions, outDoc);
		}
	}
	
	private void processFieldPermissionsNode(Node fieldPermissionNode, Element parentElement, Document outDoc){
		if (fieldPermissionNode.getNodeType() == Node.ELEMENT_NODE) {
			NodeList fieldPermissionChildNodes = fieldPermissionNode.getChildNodes();			
			for(int index=0; index<fieldPermissionChildNodes.getLength(); index++){	
				Node childNode = fieldPermissionChildNodes.item(index);
				String strFieldVal = childNode.getTextContent().trim();
				if(childNode.getNodeName() != "#text"){
					Element element = null;
					element = outDoc.createElement(childNode.getNodeName());
					if(childNode.getNodeName() == "field" && childNode.getTextContent().contains("__c")){				
						strFieldVal = processFieldNodeContent(childNode);								
					}	
					if(childNode.getNodeName() == "readable" || childNode.getNodeName() == "editable"){				
						strFieldVal = "true";								
					}	
					element.appendChild(outDoc.createTextNode(strFieldVal));
					parentElement.appendChild(element);									
				}					
			}		
		}
	}
	
	private void processLayoutAssignments(Document inputDoc, Element rootElement, Document outDoc){
		NodeList layoutAssignmentsNodeList = inputDoc.getElementsByTagName("layoutAssignments");
		for (int temp = 0; temp < layoutAssignmentsNodeList.getLength(); temp++) {
			
			Node layoutAssignmentsNode = layoutAssignmentsNodeList.item(temp);
			NodeList layoutNodeList = layoutAssignmentsNode.getChildNodes();
			Node layoutNode = layoutNodeList.item(1);			
			if(layoutNode.getTextContent().contains("__c")){
				Element layoutAssignments = outDoc.createElement("layoutAssignments");	
				rootElement.appendChild(layoutAssignments);
				processLayoutAssignmentsNode(layoutAssignmentsNode, layoutAssignments, outDoc);			
			}
		}
	}
	
	private void processLayoutAssignmentsNode(Node layoutAssignmentsNode, Element parentElement, Document outDoc){
		if (layoutAssignmentsNode.getNodeType() == Node.ELEMENT_NODE) {
			NodeList layoutAssignmentsChildNodes = layoutAssignmentsNode.getChildNodes();			
			for(int index=0; index<layoutAssignmentsChildNodes.getLength(); index++){	
				Node childNode = layoutAssignmentsChildNodes.item(index);
				String strFieldVal = childNode.getTextContent().trim();
				if(childNode.getNodeName() != "#text"){
					Element element = null;
					element = outDoc.createElement(childNode.getNodeName());
					if(childNode.getNodeName() == "layout" && childNode.getTextContent().contains("__c")){				
						strFieldVal = processLayoutNodeContent(childNode);								
					}	
					if(childNode.getNodeName() == "recordType" && childNode.getTextContent().contains("__c")){				
						strFieldVal = processRecordTypeNodeContent(childNode);								
					}
					
					element.appendChild(outDoc.createTextNode(strFieldVal));
					parentElement.appendChild(element);									
				}					
			}		
		}
	}
	
	private void processRecordTypeVisibilities(Document inputDoc, Element rootElement, Document outDoc){
		NodeList recordTypeVisibilitiesNodeList = inputDoc.getElementsByTagName("recordTypeVisibilities");
		for (int temp = 0; temp < recordTypeVisibilitiesNodeList.getLength(); temp++) {
			Element recordTypeVisibilities = outDoc.createElement("recordTypeVisibilities");	
			rootElement.appendChild(recordTypeVisibilities);
			processRecordTypeVisibilitiesNode(recordTypeVisibilitiesNodeList.item(temp), recordTypeVisibilities, outDoc);
		}
	}
	
	private void processRecordTypeVisibilitiesNode(Node recordTypeVisibilitiesNode, Element parentElement, Document outDoc){
		if (recordTypeVisibilitiesNode.getNodeType() == Node.ELEMENT_NODE) {
			NodeList recordTypeVisibilitiesChildNodes = recordTypeVisibilitiesNode.getChildNodes();			
			for(int index=0; index<recordTypeVisibilitiesChildNodes.getLength(); index++){	
				Node childNode = recordTypeVisibilitiesChildNodes.item(index);
				String strFieldVal = childNode.getTextContent().trim();
				if(childNode.getNodeName() != "#text"){
					Element element = null;
					element = outDoc.createElement(childNode.getNodeName());
					if(childNode.getNodeName() == "recordType" && childNode.getTextContent().contains("__c")){				
						strFieldVal = processRecordTypeNodeContent(childNode);								
					}
					if(childNode.getNodeName() == "visible"){				
						strFieldVal = "true";								
					}					
					element.appendChild(outDoc.createTextNode(strFieldVal));
					parentElement.appendChild(element);									
				}					
			}		
		}		
	}
	
	private String processFieldNodeContent(Node node){
		String strFieldVal = "";
		String strContent = node.getTextContent();
		String strSeparator = "";
		if(strContent.contains(".") || strContent.contains("-")){			    	
			if(strContent.contains("."))
				strSeparator = ".";
			else
				strSeparator = "-";			    	 

			String strContents [] = strContent.split("\\"+strSeparator,2);
			
			for(int strIndex=0;strIndex<strContents.length;strIndex++){					
				if(strContents[strIndex].contains("__c")){
					if(strIndex == 0)
						strFieldVal = strFieldVal + addNamespace(strContents[strIndex]);
					else
						strFieldVal = strFieldVal + strSeparator + addNamespace(strContents[strIndex]);
				}
				else{
					if(strFieldVal.isEmpty())
						strFieldVal = strFieldVal + strContents[strIndex];
					else
					{
						if(strContents[strIndex].contains("__c"))
							strFieldVal = strFieldVal + strSeparator + addNamespace(strContents[strIndex]);
						else
							strFieldVal = strFieldVal + strSeparator + strContents[strIndex];
					}					
				}
					
			}
		}
		else
			strFieldVal = strContent;
		//System.out.println("processRelatedList strFieldVal::"+strFieldVal);		
		return strFieldVal;
	}

	private String processLayoutNodeContent(Node node){
		String strFieldVal = "";
		String strContent = node.getTextContent();
		String strSeparator = "";
		if(strContent.contains(".") || strContent.contains("-")){			    	
			if(strContent.contains("."))
				strSeparator = ".";
			else
				strSeparator = "-";			    	 

			String strContents [] = strContent.split("\\"+strSeparator,2);

			for(int strIndex=0;strIndex<strContents.length;strIndex++){	
				if(strIndex == 0)
					strFieldVal = strFieldVal + addNamespace(strContents[strIndex]);
				else
					strFieldVal = strFieldVal + strSeparator + addNamespace(strContents[strIndex]);						
			}
		}
		else
			strFieldVal = strContent;				
		return strFieldVal;
	}

	private String processRecordTypeNodeContent(Node node){
		String strFieldVal = "";
		String strContent = node.getTextContent();
		String strSeparator = "";
		if(strContent.contains(".") || strContent.contains("-")){			    	
			if(strContent.contains("."))
				strSeparator = ".";
			else
				strSeparator = "-";			    	 

			String strContents [] = strContent.split("\\"+strSeparator,2);
			
			for(int strIndex=0;strIndex<strContents.length;strIndex++){			
				if(strIndex == 0)
					strFieldVal = strFieldVal + addNamespace(strContents[strIndex]);
				else					
					strFieldVal = strFieldVal + strSeparator + addNamespace(strContents[strIndex]);
			}
		}
		else
			strFieldVal = strContent;
		//System.out.println("strFieldVal::"+strFieldVal);		
		return strFieldVal;
	}

	private String convertDomToString(Document outDoc) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(outDoc);
		StreamResult result = new StreamResult(new StringWriter());
		transformer.transform(source, result);
		return result.getWriter().toString();
	}

	private Element createRootElement(Document outDoc) {
		Element rootElement = outDoc.createElement("Profile");
		Attr attr = outDoc.createAttribute("xmlns");
		attr.setValue("http://soap.sforce.com/2006/04/metadata");
		rootElement.setAttributeNode(attr);
		outDoc.appendChild(rootElement);
		return rootElement;
	}	

	private String addNamespace(String str) {
		str = SFUtils.packagePrefix+ str;
		return str;
	}
}
