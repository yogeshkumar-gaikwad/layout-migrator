package com.layout.mig.old;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import com.sforce.soap.metadata.AsyncResult;
import com.sforce.soap.metadata.FileProperties;
import com.sforce.soap.metadata.ListMetadataQuery;
import com.sforce.soap.metadata.MetadataConnection;
import com.sforce.soap.metadata.PackageTypeMembers;
import com.sforce.soap.metadata.RetrieveRequest;
import com.sforce.soap.metadata.RetrieveResult;
import com.sforce.soap.metadata.StaticResource;
import com.sforce.soap.metadata.StaticResourceCacheControl;
import com.sforce.soap.metadata.UpdateMetadata;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

public class LayoutHelper {

	// Currently just supports a single object for testing
//	public static String userName = "pankaj@gnt.pack";
//	public static String password = "test@1234";	
	public static String userName = "packageorg1@reisystems.in";
	public static String password = "test@12345";	
	public static String strStaticResourceName = "ConfigPageLayouts";
	public static String sourceObject = "*";	
	public static String sourceProfile = "Admin";
	public static String sourceLayout = "*";
	//public static String packagePrefix = "GNT";
	public static String packagePrefix = "PKGORG1";
	public static Set<String> setCustomFields = new HashSet<String>();
	static PartnerConnection connection;	
	Error error = new Error();	
	public static boolean IsHelpText = false;
	public static boolean isActionOverrides = false;
	public static Set<String> cleanedLayouts;
	public static String [] strAllowedFieldTags = {
		"<CustomObject xmlns=\"http://soap.sforce.com/2006/04/metadata\">",
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>","</CustomObject>",
		"<fields>",
		"</fields>",
		"<fullName>",
		"<inlineHelpText>",
		"<label>",
		"<type>",
		"<pluralLabel>",
		"<referenceTo>",
		"<precision>",
		"<nameField>",
		"</nameField>",
		"<deploymentStatus>",
		"</deploymentStatus>",
		"<sharingModel>",
		"</sharingModel>"
	};
	
	

	
	public LayoutHelper(){
		
	}
	
	public static boolean isAllowedFieldTag(String strLine){
		boolean isAllowed = false;
		for(int index=0;index<strAllowedFieldTags.length;index++){
			if(strLine.contains(strAllowedFieldTags[index])){
				isAllowed = true;
				break;
			}				
		}
		return isAllowed;
	}
	/* 
	public static void initCleanedLayouts(){
		String strLayouts[] = { 
				"PageLayoutConfig__c",
				"PageLayoutActionConfig__c",
				"TabConfig__c",
				"PageBlockConfig__c",
				"PageBlockDetailConfig__c",
				"PageAttachmentConfig__c",
				"LayoutBusinessRuleConfig__c",
				"SObjectLayoutConfig__c",
				"DataTableConfig__c",
				"DataTableAction__c",
				"DataTableDetailConfig__c",
				"FlexTableFilterListViewConfig__c",
				"FlexGridConfig__c",
				"StepProgressbarHeader__c",
				"StepProgressbarDetail__c",
				"ApprovalProcessStepCache__c"
				
		};
		cleanedLayouts = new HashSet<>(Arrays.asList(strLayouts));		
	}*/
	
	
	public static void initCleanedLayouts(){
		String strLayouts[] = { "Employee__c", "Project__c" };
		cleanedLayouts = new HashSet<>(Arrays.asList(strLayouts));		
	}
	public static boolean isCleanedLayout(ZipEntry zipEntry){
		boolean layoutFlag = false;
		for (String cleanedLayout : cleanedLayouts) {
			if(zipEntry.getName().contains(cleanedLayout)){
				layoutFlag = true;
				break;
			}						    
		}
		return layoutFlag;			
	}
	
	
	// Setup salesforce metadata connection
    public static MetadataConnection setupConnection() throws Exception{
    	initCleanedLayouts();
    	
    	ConnectorConfig partnerConfig = new ConnectorConfig();
	    ConnectorConfig metadataConfig = new ConnectorConfig();	    
	    loadCredentials(partnerConfig,userName,password);
	    //partnerConfig.setTraceMessage(true);
	    //loadCredentials(partnerConfig,"pankaj@ggp.dev5","test@12345678");
	    @SuppressWarnings("unused")
	    PartnerConnection partnerConnection = com.sforce.soap.partner.Connector.newConnection(partnerConfig);
	    
	    // shove the partner's session id into the metadata configuration then connect
	    metadataConfig.setSessionId(partnerConnection.getSessionHeader().getSessionId());
	    MetadataConnection metadataConnection = com.sforce.soap.metadata.Connector.newConnection(metadataConfig);
	    return metadataConnection;
    }
    // loading salesforce credentials
    private static void loadCredentials(ConnectorConfig partnerConfig,String strSFUserName, String strSFPwd) throws IOException
	{
		//Properties prop = new Properties();
		InputStream input = null;		
		try
		{
			//input = new FileInputStream("config.properties");
			//prop.load(input);
		    partnerConfig.setUsername(strSFUserName);//prop.getProperty("username"));
		    partnerConfig.setPassword(strSFPwd);//prop.getProperty("password"));
		}
		catch (Exception e) {
			if (input != null) {
				input.close();
			}
			throw e;
		}
	}
    public static void processProfiles() throws Exception {	
    	MetadataConnection metadataConnection = setupConnection();
		RetrieveRequest retrieveRequest = new RetrieveRequest();
		retrieveRequest.setSinglePackage(true);
		com.sforce.soap.metadata.Package packageManifest = new com.sforce.soap.metadata.Package();
		ArrayList<PackageTypeMembers> types = new ArrayList<PackageTypeMembers>();
		
		PackageTypeMembers packageTypeMember = new PackageTypeMembers();			
		packageTypeMember.setName("Profile");
		packageTypeMember.setMembers(new String[] { sourceProfile });
		types.add(packageTypeMember);
		
		packageTypeMember = new PackageTypeMembers();
		packageTypeMember.setName("Layout");
		packageTypeMember.setMembers(new String[] { sourceLayout });
		types.add(packageTypeMember);
		
		packageTypeMember = new PackageTypeMembers();
		packageTypeMember.setName("CustomObject");
		packageTypeMember.setMembers(cleanedLayouts.toArray(new String[0]));		
		types.add(packageTypeMember);
		
		packageManifest.setTypes((PackageTypeMembers[]) types.toArray(new PackageTypeMembers[] {}));			
		retrieveRequest.setUnpackaged(packageManifest);
		AsyncResult response = metadataConnection.retrieve(retrieveRequest);	
		while(!response.isDone()) {
		    Thread.sleep(1000);
		    response = metadataConnection.checkStatus(new String[] { response.getId()} )[0];
		}
		RetrieveResult retrieveResult = metadataConnection.checkRetrieveStatus(response.getId());				
		byte[] zipBytes = packagingLayoutAssignmentElements(retrieveResult);		
		deployStaticResource(metadataConnection,zipBytes);		
    }
    private static String manageProfileEntry(ZipInputStream zipis, ZipEntry zipEntry) throws Exception{
    	String xmlFile = "";
    	String strZipEntryName = zipEntry.getName();		  
     	Scanner sc = new Scanner(zipis);
     	boolean userPermissionFlag = false;
     	String strTempLayout = "";
     	boolean isCustomLayout = false;
         while (sc.hasNextLine()) {	            
         	String strLine = new String();	            	
         	strLine = sc.nextLine();	            
         	if("<layoutAssignments>".equals(strLine.trim()))
         		strTempLayout = "<layoutAssignments>";
         	if(strLine.contains("<userPermissions>"))
         		userPermissionFlag = true;	 
         	if(!userPermissionFlag){	            		
         		if(strLine.contains("__c")){
         			if(strTempLayout.length()>0){
         				xmlFile = xmlFile + strTempLayout + appendNamespaceToField(strLine);         				
         				strTempLayout = "";
         			}	            				
         			else
         				xmlFile = xmlFile + appendNamespaceToField(strLine);
         		}
         		else{	            			
         			if(strTempLayout.length() == 0)
         				xmlFile = xmlFile + strLine;
         			if("</layoutAssignments>".equals(strLine.trim()))
         				strTempLayout = "";
         		}		               		           	
         	}
         	else
         	{
         		if(strLine.contains("</userPermissions>"))
         			userPermissionFlag = false;	
         	}  
         }	
    	return xmlFile;
    }
    public static byte[] packagingLayoutAssignmentElements(RetrieveResult retrieveResult) throws Exception {    	
	    byte[] zipBytes = retrieveResult.getZipFile();	
		ZipInputStream zipis = new ZipInputStream(new ByteArrayInputStream(zipBytes, 0, zipBytes.length));
		ZipEntry zipEntry = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream out = new ZipOutputStream(baos);		 
		String strMembers = "";
		String xmlProfile = "";
		String strMembersFinal= "";		
		while((zipEntry = zipis.getNextEntry()) != null) {			
			if(isCleanedLayout(zipEntry) || zipEntry.getName().contains("profiles/")){
				String xmlFile = "";
				boolean isProfile = false;
				boolean isObjects = false;
				boolean isStandardLayout = false;
				
				String strFinalEntryWithPrefix = zipEntry.getName();	
				//System.out.println("out zipEntry.getName()----" + zipEntry.getName());
				
				if(zipEntry.getName().contains("objects/") ) {
					System.out.println("In Objects");
					isObjects = true;	
					System.out.println("This is Object::");
				}	
				
				
				if(zipEntry.getName().contains("profiles/") ) {
					isProfile = true;
					xmlFile = manageProfileEntry(zipis,zipEntry);
				}			
				
				if(zipEntry.getName().contains("__c")) {				
				
					String strZipEntryName = zipEntry.getName();
					if(packagePrefix.length()>0){	
						String strTemp = "";
						String strEntryWithPrefix = "";					
						strEntryWithPrefix = strZipEntryName.split("/",2)[0] + "/"+ packagePrefix +"__"+ strZipEntryName.split("/")[1];
						if(!isObjects){
							System.out.println("This is Layout::");
							String[] layoutArray = strEntryWithPrefix.split("-",2); 							
							for(int i=0; i<layoutArray.length; i++) {
								if(i==1) {
									strTemp += "-" + packagePrefix + "__" + layoutArray[i];
								} else {
									strTemp += "-"+layoutArray[i];
								}
							}								
							if(strTemp.substring(1).contains("-")){
								strFinalEntryWithPrefix = "";
								strFinalEntryWithPrefix = strTemp.substring(1, strTemp.length());						
							}	
						}
						else
							strFinalEntryWithPrefix = strEntryWithPrefix;	
					}
					Scanner sc = new Scanner(zipis);			
				
					boolean isRecordType = false;
					boolean isValidationRule = false;
					boolean isWebLink = false;
				//	boolean isNameField = false;
				    boolean isPicklist = false;
				    boolean isListViews = false;
				    boolean isPicklistValues = false;
				  
					while (sc.hasNextLine()) {						
						String strLine = new String();	            	
						strLine = sc.nextLine();	
						if(isObjects){	
							
							if(strLine.contains("<actionOverrides>"))
								isActionOverrides = true;
							if(strLine.contains("<recordTypes>"))
								isRecordType = true;
							if(strLine.contains("<validationRules>"))
								isValidationRule = true;
							if(strLine.contains("<webLinks>"))
								isWebLink = true;
//							if(strLine.contains("<nameField>"))
//								isNameField = true;
							if(strLine.contains("<picklist>"))
								isPicklist = true;
							if(strLine.contains("<listViews>"))
								isListViews = true;
							if(strLine.contains("<picklistValues>"))
								isPicklistValues = true;
							
							if(!isActionOverrides && !isRecordType && !isValidationRule && !isWebLink && !isPicklist && !isListViews && !isPicklistValues){
								if(isAllowedFieldTag(strLine)){
									if(strLine.contains("__c"))
										xmlFile = xmlFile + appendNamespaceToField(strLine);
									else
										xmlFile = xmlFile + strLine;
								}							
							}	
							if(strLine.contains("</actionOverrides>"))
								isActionOverrides = false;
							if(strLine.contains("</recordTypes>"))
								isRecordType = false;
							if(strLine.contains("</validationRules>"))
								isValidationRule = false;
							if(strLine.contains("</webLinks>"))
								isWebLink = false;
//							if(strLine.contains("</nameField>"))
//								isNameField = false;
							if(strLine.contains("</picklist>"))
								isPicklist = false;
							if(strLine.contains("</listViews>"))
								isListViews = false;
							if(strLine.contains("</picklistValues>"))
								isPicklistValues = true;
							
							
						}
						else{
						
							if(strLine.contains("__c") || strLine.contains("<customButtons>") || strLine.contains("<page>"))
								xmlFile = xmlFile + appendNamespaceToField(strLine);													
							else
								xmlFile = xmlFile + strLine;
						}						
					
					}				
				}				
				else
					isStandardLayout = isProfile ? false : true;
				if(!isStandardLayout){
					if(isProfile || !strFinalEntryWithPrefix.contains("package.xml")){
						//System.out.println("strFinalEntryWithPrefix::"+strFinalEntryWithPrefix);
				        ZipEntry e = new ZipEntry(strFinalEntryWithPrefix);
				        out.putNextEntry(e);
				      //  System.out.println("xmlFile::"+xmlFile);
				        byte[] data = xmlFile.getBytes();
				        out.write(data, 0, data.length);
						out.closeEntry();		
					}				
				}
			}
		}
		ZipEntry e = new ZipEntry("package.xml");
		out.putNextEntry(e);       
        String strXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Package xmlns=\"http://soap.sforce.com/2006/04/metadata\"><types><members>Admin</members><name>Profile</name></types><types><members>*</members><name>Layout</name></types><types>"+buildXmlCustomObjectMembers()+"<name>CustomObject</name></types><version>43.0</version></Package>";
        byte[] data = strXml.getBytes(); 
        out.write(data, 0, data.length);
		out.closeEntry();
		out.close();			
		zipBytes = baos.toByteArray();
		return zipBytes;
	}	
    public static void processLayouts() throws Exception {				
		MetadataConnection metadataConnection = setupConnection();
		RetrieveRequest retrieveRequest = new RetrieveRequest();
		retrieveRequest.setSinglePackage(true);
		com.sforce.soap.metadata.Package packageManifest = new com.sforce.soap.metadata.Package();
		ArrayList<PackageTypeMembers> types = new ArrayList<PackageTypeMembers>();
		PackageTypeMembers packageTypeMember = new PackageTypeMembers();
		
		packageTypeMember.setName("Layout");
		packageTypeMember.setMembers(new String[] { sourceObject });
		types.add(packageTypeMember);
		
		/*packageTypeMember = new PackageTypeMembers();
		packageTypeMember.setName("CustomObject");
		packageTypeMember.setMembers(new String[] { sourceObject });		
		types.add(packageTypeMember);
		
		packageTypeMember = new PackageTypeMembers();
		packageTypeMember.setName("Profile");
		packageTypeMember.setMembers(new String[] { sourceProfile });		
		types.add(packageTypeMember);*/
		
		packageManifest.setTypes((PackageTypeMembers[]) types.toArray(new PackageTypeMembers[] {}));			
		retrieveRequest.setUnpackaged(packageManifest);
		AsyncResult response = metadataConnection.retrieve(retrieveRequest);	
		while(!response.isDone()) {
		    Thread.sleep(1000);
		    response = metadataConnection.checkStatus(new String[] { response.getId()} )[0];
		}
		RetrieveResult retrieveResult = metadataConnection.checkRetrieveStatus(response.getId());		
		byte[] zipBytes = packagingElements(retrieveResult);		
		deployStaticResource(metadataConnection,zipBytes);
	}
	public static boolean isResourceAvailable(RetrieveResult retrieveResult, String strResourceName) throws Exception {
    	System.out.println("strResourceName to search:: "+strResourceName);
    	boolean isAvailable = false;
    	for(FileProperties fileProperty : retrieveResult.getFileProperties()){
    		System.out.println(fileProperty.getFullName());
    		if(fileProperty.getFullName().equalsIgnoreCase(strResourceName)){
    			isAvailable = true;
    			break;
    		}    			
	        else
	        	isAvailable = false;
    	}    	
    	return isAvailable;
    }
	private static String buildXmlCustomObjectMembers(){	
		System.out.println("In buildXmlCustomFieldMembers-------");
		String[] splitMembers = cleanedLayouts.toArray(new String[0]);
		System.out.println("splitMembers-------"+splitMembers);
		 String strMembersTag = "";
		for(int i=0;i<splitMembers.length;i++) {			
			strMembersTag = strMembersTag+ "<members>"+ packagePrefix + "__" +splitMembers[i]+"</members>";
	    }
		 //String strMembersTag1 = strMembersTag.split("/")[1];
		 System.out.println("memberList------->"+strMembersTag);
		 return strMembersTag;
		 
	}
    public static byte[] packagingElements(RetrieveResult retrieveResult) throws Exception {				
	    byte[] zipBytes = retrieveResult.getZipFile();	
		ZipInputStream zipis = new ZipInputStream(new ByteArrayInputStream(zipBytes, 0, zipBytes.length));
		 
		ZipEntry zipEntry = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream out = new ZipOutputStream(baos);	
		 
		String strMembers = "";
		String xmlProfile = "";
		String strMembersFinal= "";
	
		boolean skipXml = false;
		boolean miniLayoutEndFlag = false;
		while((zipEntry = zipis.getNextEntry()) != null) {	
			
			
			String strFinalEntryWithPrefix = zipEntry.getName();			
			if(zipEntry.getName().contains("__c") ) {	
		        System.out.println("zipEntry.getName()----" + zipEntry.getName());
			    String strZipEntryName = zipEntry.getName();
			    if(packagePrefix.length()>0){
			    
			      String strEntryWithPrefix = "";
			      strEntryWithPrefix = strZipEntryName.split("/")[0] + "/"+ packagePrefix +"__"+ strZipEntryName.split("/")[1];
		    	  String[] layoutArray = strEntryWithPrefix.split("-"); 
				  strFinalEntryWithPrefix = "";
				  for(int i=0; i<layoutArray.length; i++) {
					  if(i==1) {
			    		strFinalEntryWithPrefix += "-" + packagePrefix + "__" + layoutArray[i];
					  } else {
			    		strFinalEntryWithPrefix += "-"+layoutArray[i];
					  }
				  }				
				  strFinalEntryWithPrefix = strFinalEntryWithPrefix.substring(1, strFinalEntryWithPrefix.length());
				   
			    }
			    
			   // strMembers = strMembers + packagePrefix +"__"+ strZipEntryName.split("/")[1]+',';
			  
		        
			    String xmlFile = "";         
            	Scanner sc = new Scanner(zipis);
	            while (sc.hasNextLine()) {
	            	String strLine = new String();	            	
	            	strLine = sc.nextLine();	            
	            	//System.out.println("strLine::"+strLine);
	            	
	            	if(strLine.contains("__c"))            		     		
	            		xmlFile = xmlFile + appendNamespaceToField(strLine);
	            	else
	            		xmlFile = xmlFile + strLine;	                   	
	            }
		        System.out.println(xmlFile);		 
		        //System.out.println("strFinalEntryWithPrefix------>"+strFinalEntryWithPrefix);		        
		        ZipEntry e = new ZipEntry(strFinalEntryWithPrefix);
		        out.putNextEntry(e);		        
		        byte[] data = xmlFile.getBytes();
		        out.write(data, 0, data.length);
				out.closeEntry();				
		    }
			/*if(zipEntry.getName().contains("profiles/Cloned System Admin.profile")) {
				 String xmlFile = "";         
	            	Scanner sc = new Scanner(zipis);
		            while (sc.hasNextLine()) {
		            	String strLine = new String();
		            	strLine = sc.nextLine(); 
	            		xmlFile = xmlFile + strLine;
		            }
			        System.out.println(xmlFile);		 
			        ZipEntry e = new ZipEntry("profiles/Cloned System Admin");
			        out.putNextEntry(e);
			        
			        byte[] data = xmlFile.getBytes();
			        out.write(data, 0, data.length);
					out.closeEntry();	
			}*/
			
			
		}
		
		/*if(strFinalEntryWithPrefix.length() > 0 ){
			strMembersFinal= strFinalEntryWithPrefix.substring(0,strFinalEntryWithPrefix.length()-1);			
			System.out.println("strMembersFinal---->" + strMembersFinal);
		}*/

		ZipEntry e = new ZipEntry("package.xml");
        out.putNextEntry(e);
       
        //String strXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><Package xmlns=\"http://soap.sforce.com/2006/04/metadata\"><types>"+buildXmlMembers(strMembers)+ "<name>Layout</name></types><types><members>*</members><name>CustomObject</name></types><types><members>Cloned System Admin</members><name>Profile</name></types><version>43.0</version></Package>";
        String strXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Package xmlns=\"http://soap.sforce.com/2006/04/metadata\"><types><members>*</members><name>Layout</name></types><version>43.0</version></Package>";
        System.out.println("strXml::"+strXml);
        byte[] data = strXml.getBytes(); 
        out.write(data, 0, data.length);
		out.closeEntry();
		out.close();			
		zipBytes = baos.toByteArray();
		return zipBytes;
	}
	private static boolean checkComponentPresent(String strLine, String componentTag){
		boolean isComponentPresent = false;
		if(strLine.contains(componentTag))
			isComponentPresent = true;
    	return isComponentPresent;   	
	}
	private static boolean checkForMiniLayout(String strLine){
		boolean miniLayoutFlag = false;
		if(strLine.contains("<miniLayout>"))
    		miniLayoutFlag = true;
    	return miniLayoutFlag;
	}
	private static void deployStaticResource(MetadataConnection metadataConnection, byte[] zipBytes) throws Exception{
	    
		StaticResource sr = new StaticResource();
	    sr.setFullName("ConfigPageLayouts");
	    sr.setCacheControl(StaticResourceCacheControl.Public);
	    sr.setContentType("application/zip");
	    sr.setContent(zipBytes);
	    
	    UpdateMetadata updateMetadata = new UpdateMetadata();
    	updateMetadata.setCurrentName("ConfigPageLayouts");
    	updateMetadata.setMetadata(sr);
    	AsyncResult[] results = new AsyncResult []{};
	    results = metadataConnection.update(new UpdateMetadata[]{updateMetadata});
	
	 
	    for(AsyncResult result : results ){
	    	while(!result.isDone()) {
			    Thread.sleep(1000);
			    result = metadataConnection.checkStatus(new String[] { result.getId()} )[0];
			}
	    }
	}
	private static boolean IsValidXml(String strLine){
		boolean isValid = false;	
		String strTwo = strLine.length() < 2 ? strLine : strLine.substring(0, 2);
		if(strTwo.contains("</")){
			return false;
		}
			
		if(strLine.contains("<") && strLine.contains(">") && strLine.contains("</"))
			isValid = true;
		/*if(strLine.contains("<inlineHelpText>"))
			isValid = false;
		if(strLine.contains("</inlineHelpText>"))
			isValid = false;*/
		return isValid; 	
	}
	private static String appendNamespaceToField(String strLine) throws Exception
	{
		String strSeparator = "";	
		// System.out.println("IsValidXml(strLine)::"+IsValidXml(strLine));
		if(packagePrefix.length()>0 && IsValidXml(strLine.trim())){			
			 String xmlStartTag = strLine.substring(0,strLine.indexOf(">")+1).trim();			 
		     String xmlEndTag = xmlStartTag.replace("<", "</").trim(); 
		     String strFinalContent = "";
		     String strContent = "";
		     if(!xmlStartTag.isEmpty() && !xmlEndTag.isEmpty()){
		    	// System.out.println("strLine::"+strLine);
		    	 String strSub1 = strLine.substring(strLine.indexOf(">")+1,strLine.length());			   
			     System.out.println("strLine::"+strLine);
			     System.out.println("strSub1::"+strSub1);
			     System.out.println("xmlStartTag::"+xmlStartTag);
			     System.out.println("xmlEndTag::"+xmlEndTag);
		    	 System.out.println("strSub1::"+strSub1);
		    //	 if(!strSub1.isEmpty())
			       strContent = strSub1.substring(0,strSub1.indexOf("<"));		
			     System.out.println("strContent::"+strContent);
			     
			     if(xmlStartTag.contains("<customButtons>")){
			    	// System.out.println("strLine::"+strLine);
					// System.out.println("strSub1::"+strSub1);
					// System.out.println("xmlStartTag::"+xmlStartTag);
					// System.out.println("xmlEndTag::"+xmlEndTag);
			     }
			    	
			     if(strContent.contains(".") || strContent.contains("-")){			    	
			    	if(strContent.contains("."))
			    		strSeparator = ".";
			    	else
			    		strSeparator = "-";
			    	 
			    //	System.out.println("strSeparator::"+strSeparator);
			    	 String strContents [] = strContent.split("\\"+strSeparator,2);
		            for(int index=0;index<strContents.length;index++){	             
	                    if(index == 0)
	                         strFinalContent = strFinalContent + packagePrefix +"__" + strContents[index];
	                    else
	                    {
	                    	strFinalContent = strFinalContent + strSeparator + packagePrefix +"__" + strContents[index];
	                    	/*
	                    	if(strContents[index].contains("__c")){
	                    		strFinalContent = strFinalContent + strSeparator + packagePrefix +"__" + strContents[index];
	                    	}
	                    	else
	                    		strFinalContent = strFinalContent + strSeparator + strContents[index];
	                    	*/
	                    }
	                                       	                   
	                   // if(strContents[index].contains("__c") && xmlStartTag.equals("<field>"))
	                    	//setCustomFields.add(strContents[index]);
		            }
			     }
			     else{
			    	 if(strContent.contains("__c"))
			    		 strFinalContent = strFinalContent + packagePrefix + "__" + strContent;
			    	 else
			    		 strFinalContent = strContent;
			     }
			    	 
			        	//strFinalContent = strFinalContent + packagePrefix +"__" + strContent; 
		     
	        }
	        
	        strFinalContent = xmlStartTag + strFinalContent.trim() + xmlEndTag;	   
	        System.out.println("strFinalContent::"+strFinalContent);
	        return strFinalContent;
		}	
		else
			return strLine;
	}
	public static void listMetadata(String strResourceType) throws Exception {
		  try {
		    MetadataConnection metadataConnection = setupConnection();
			RetrieveRequest retrieveRequest = new RetrieveRequest();
		    ListMetadataQuery query = new ListMetadataQuery();
		    query.setType(strResourceType);
		    
		    //query.setFolder(null);
		    double asOfVersion = 43.0;
		    // Assuming that the SOAP binding has already been established.
		    FileProperties[] lmr = metadataConnection.listMetadata(
		        new ListMetadataQuery[] {query}, asOfVersion);
		    if (lmr != null) {
		      for (FileProperties n : lmr) {
		    	
		        System.out.println("Component fullName: " + n);
		        System.out.println("Component type: " + n.getType());
		      }
		    }            
		  } catch (ConnectionException ce) {
		    ce.printStackTrace();
		  }
		}
	public static void main(String[] args) throws Exception {
		//processLayouts();	
		processProfiles();
		
		System.out.println("Setup Layouts in Static Resources");
		System.out.println("setCustomFields::"+setCustomFields);
		//listMetadata("Profile Layout Assignment");
		
		
		//RetrieveResult retrieveResult = fetchPackedResources("Layout","GNT");
		/*String [] strResources = new String[] {"Admin"} ;
		RetrieveResult retrieveResult = fetchResources("Profile",strResources);
		for(FileProperties fileProperty : retrieveResult.getFileProperties()){
			//if(fileProperty.getNamespacePrefix().equals("GNT"))
				System.out.println(fileProperty);
		}*/
		
		
	}
	public static RetrieveResult fetchResources(String strResourceType, String [] strResources) throws Exception {
	    MetadataConnection metadataConnection = setupConnection();
	    
	    // Retrieve Custom Object Meta data for Source Object
		RetrieveRequest retrieveRequest = new RetrieveRequest();
		retrieveRequest.setSinglePackage(true);
		com.sforce.soap.metadata.Package packageManifest = new com.sforce.soap.metadata.Package();
		ArrayList<PackageTypeMembers> types = new ArrayList<PackageTypeMembers>();
		PackageTypeMembers packageTypeMember = new PackageTypeMembers();
		packageTypeMember.setName(strResourceType);
		packageTypeMember.setMembers(strResources);
		types.add(packageTypeMember);
		packageManifest.setTypes((PackageTypeMembers[]) types.toArray(new PackageTypeMembers[] {}));			
		retrieveRequest.setUnpackaged(packageManifest);
		AsyncResult response = metadataConnection.retrieve(retrieveRequest);
		while(!response.isDone()) {
		    Thread.sleep(1000);
		    response = metadataConnection.checkStatus(new String[] { response.getId()} )[0];
		}
	    RetrieveResult retrieveResult = metadataConnection.checkRetrieveStatus(response.getId());
		return retrieveResult;
		 
	}    
	public static RetrieveResult fetchPackedResources(String strResourceType, String packageName) throws Exception {
	    MetadataConnection metadataConnection = setupConnection();
	    
	    // Retrieve Custom Object Meta data for Source Object
		RetrieveRequest retrieveRequest = new RetrieveRequest();
		retrieveRequest.setPackageNames(new String[] { packageName });
		//retrieveRequest.setSinglePackage(false);
		com.sforce.soap.metadata.Package packageManifest = new com.sforce.soap.metadata.Package();
		ArrayList<PackageTypeMembers> types = new ArrayList<PackageTypeMembers>();
		PackageTypeMembers packageTypeMember = new PackageTypeMembers();
		packageTypeMember.setName(strResourceType);
		
		
		packageTypeMember.setMembers(new String[] { sourceObject });
		types.add(packageTypeMember);
		packageManifest.setTypes((PackageTypeMembers[]) types.toArray(new PackageTypeMembers[] {}));			
		retrieveRequest.setUnpackaged(packageManifest);
		AsyncResult response = metadataConnection.retrieve(retrieveRequest);
		while(!response.isDone()) {
		    Thread.sleep(1000);
		    response = metadataConnection.checkStatus(new String[] { response.getId()} )[0];
		}
	    RetrieveResult retrieveResult = metadataConnection.checkRetrieveStatus(response.getId());
		return retrieveResult;		 
	}    
}
