package com.layout.mig.old;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;

public class Processor {
	
	
	public static Set<String> cleanedObjects;
	public static void initCleanedObjects(){
		String strLayouts[] = { "Employee__c", "Project__c" };
		cleanedObjects = new HashSet<>(Arrays.asList(strLayouts));		
	}
	public static boolean isCleanedLayout(ZipEntry zipEntry){
		boolean layoutFlag = false;
		for (String cleanedObject : cleanedObjects) {
			if(zipEntry.getName().contains(cleanedObject)){
				layoutFlag = true;
				break;
			}						    
		}
		return layoutFlag;			
	}
}
