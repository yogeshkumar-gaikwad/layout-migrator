package com.layout.mig.old;
import java.io.ByteArrayInputStream;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import com.sforce.soap.metadata.RetrieveResult;

public class LayoutProcessor extends Processor {

	private static boolean IsValidXmlTag(String strLine){
		
		String subStrLine = strLine.length() < 2 ? strLine : strLine.substring(0, 2);
		if(subStrLine.contains("</"))
			return false;
		else
			return true;
			
	}

	private static String appendNamespace(String strLine, String packagePrefix){

		if(packagePrefix.length()>0 && IsValidXmlTag(strLine)){			
			String xmlStartTag = strLine.substring(0,strLine.indexOf(">")+1).trim();			 
			String xmlEndTag = xmlStartTag.replace("<", "</").trim(); 
			String strFinalContent = "";
			String strContent = "";
			String strSub1 = strLine.substring(strLine.indexOf(">")+1,strLine.length());
			strContent = strSub1.substring(0,strSub1.indexOf("<"));
			String strSeparator = "";
			if(strContent.contains(".") || strContent.contains("-")){			    	
				if(strContent.contains("."))
					strSeparator = ".";
				else
					strSeparator = "-";	    	 

				String strContents [] = strContent.split("\\"+strSeparator,2);
				for(int index=0;index<strContents.length;index++){	             
					if(index == 0)
						strFinalContent = strFinalContent + packagePrefix +"__" + strContents[index];
					else                    
						strFinalContent = strFinalContent + strSeparator + packagePrefix +"__" + strContents[index];	                 
				}
			}
			else{
				if(strContent.contains("__c"))
					strFinalContent = strFinalContent + packagePrefix + "__" + strContent;
				else
					strFinalContent = strContent;
			}	        
			strFinalContent = xmlStartTag + strFinalContent.trim() + xmlEndTag;	   
			System.out.println("strFinalContent::"+strFinalContent);
			return strFinalContent;	   
		}	
		else
			return strLine;
	}		

	public static void handleLayoutPackaging(ZipOutputStream out, RetrieveResult retrieveResult,String packagePrefix) throws Exception {    	
		initCleanedObjects();
		byte[] zipBytes = retrieveResult.getZipFile();	
		ZipInputStream zipis = new ZipInputStream(new ByteArrayInputStream(zipBytes, 0, zipBytes.length));
		ZipEntry zipEntry = null;			
		while((zipEntry = zipis.getNextEntry()) != null) {			
			if(isCleanedLayout(zipEntry) && zipEntry.getName().contains("layouts/")){
				String xmlFile = "";
				String strFinalEntryWithPrefix = zipEntry.getName();				
				if(zipEntry.getName().contains("__c")) {
					String strZipEntryName = zipEntry.getName();
					if(packagePrefix.length()>0){	
						String strTemp = "";
						String strEntryWithPrefix = "";					
						strEntryWithPrefix = strZipEntryName.split("/",2)[0] + "/"+ packagePrefix +"__"+ strZipEntryName.split("/")[1];
						String[] layoutArray = strEntryWithPrefix.split("-",2); 							
						for(int i=0; i<layoutArray.length; i++) {
							if(i==1) {
								strTemp += "-" + packagePrefix + "__" + layoutArray[i];
							} else {
								strTemp += "-"+layoutArray[i];
							}
						}								
						if(strTemp.substring(1).contains("-")){
							strFinalEntryWithPrefix = "";
							strFinalEntryWithPrefix = strTemp.substring(1, strTemp.length());						
						}
						else
							strFinalEntryWithPrefix = strEntryWithPrefix;	
					}
					Scanner sc = new Scanner(zipis);
					while (sc.hasNextLine()) {						
						String strLine = new String();	            	
						strLine = sc.nextLine();
						
						if(strLine.contains("__c") || strLine.contains("<customButtons>") || strLine.contains("<page>"))
							xmlFile = xmlFile + appendNamespace(strLine,packagePrefix);													
						else
							xmlFile = xmlFile + strLine;
					}				
				}
				ZipEntry e = new ZipEntry(strFinalEntryWithPrefix);
				out.putNextEntry(e);
				byte[] data = xmlFile.getBytes();
				out.write(data, 0, data.length);
				out.closeEntry();			
			}
		}		
	}
}

