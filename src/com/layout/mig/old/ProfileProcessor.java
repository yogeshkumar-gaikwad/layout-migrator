package com.layout.mig.old;
import java.io.ByteArrayInputStream;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import com.sforce.soap.metadata.RetrieveResult;

public class ProfileProcessor extends Processor {
	private static boolean IsValidXmlTag(String strLine){
		String subStrLine = strLine.length() < 2 ? strLine : strLine.substring(0, 2);
		if(subStrLine.contains("</"))
			return false;
		else
			return true;
	}
	private static String appendNamespace(String strLine, String packagePrefix){
		
		if(packagePrefix.length()>0 && IsValidXmlTag(strLine)){			
			 String xmlStartTag = strLine.substring(0,strLine.indexOf(">")+1).trim();			 
		     String xmlEndTag = xmlStartTag.replace("<", "</").trim(); 
		     String strFinalContent = "";
		     String strContent = "";
			 String strSub1 = strLine.substring(strLine.indexOf(">")+1,strLine.length());
			 strContent = strSub1.substring(0,strSub1.indexOf("<"));
			 String strSeparator = "";
			 if(strContent.contains(".") || strContent.contains("-")){			    	
				 if(strContent.contains("."))
					 strSeparator = ".";
				 else
					 strSeparator = "-";	    	 

				 String strContents [] = strContent.split("\\"+strSeparator,2);
				 for(int index=0;index<strContents.length;index++){	             
					 if(index == 0)
						 strFinalContent = strFinalContent + packagePrefix +"__" + strContents[index];
					 else                    
						 strFinalContent = strFinalContent + strSeparator + packagePrefix +"__" + strContents[index];	                 
				 }
			 }
			 else{
				 if(strContent.contains("__c"))
					 strFinalContent = strFinalContent + packagePrefix + "__" + strContent;
				 else
					 strFinalContent = strContent;
			 }	        
	        strFinalContent = xmlStartTag + strFinalContent.trim() + xmlEndTag;	   
	        System.out.println("strFinalContent::"+strFinalContent);
	        return strFinalContent;	   
		}	
		else
			return strLine;
	}
	private static String manageProfileEntry(ZipInputStream zipis, ZipEntry zipEntry, String packagePrefix) throws Exception{
		String xmlFile = "";		  
		Scanner sc = new Scanner(zipis);
		boolean userPermissionFlag = false;
		String strTempLayout = "";	
		while (sc.hasNextLine()) {	            
			String strLine = new String();	            	
			strLine = sc.nextLine();	            
			if("<layoutAssignments>".equals(strLine.trim()))
				strTempLayout = "<layoutAssignments>";
			if(strLine.contains("<userPermissions>"))
				userPermissionFlag = true;	 
			if(!userPermissionFlag){	            		
				if(strLine.contains("__c")){
					if(strTempLayout.length()>0){
						xmlFile = xmlFile + strTempLayout + appendNamespace(strLine,packagePrefix);         				
						strTempLayout = "";
					}	            				
					else
						xmlFile = xmlFile + appendNamespace(strLine,packagePrefix);
				}
				else{	            			
					if(strTempLayout.length() == 0)
						xmlFile = xmlFile + strLine;
					if("</layoutAssignments>".equals(strLine.trim()))
						strTempLayout = "";
				}		               		           	
			}
			else
			{
				if(strLine.contains("</userPermissions>"))
					userPermissionFlag = false;	
			}  
		}	
		return xmlFile;
	}
	public static void handleProfilePackaging(ZipOutputStream out, RetrieveResult retrieveResult,String packagePrefix) throws Exception {    	
		initCleanedObjects();
		byte[] zipBytes = retrieveResult.getZipFile();	
		ZipInputStream zipis = new ZipInputStream(new ByteArrayInputStream(zipBytes, 0, zipBytes.length));
		ZipEntry zipEntry = null;			
		while((zipEntry = zipis.getNextEntry()) != null) {			
			if(isCleanedLayout(zipEntry) || zipEntry.getName().contains("profiles/")){
				String xmlFile = "";			
				if(zipEntry.getName().contains("profiles/") ) {				
					xmlFile = manageProfileEntry(zipis,zipEntry,packagePrefix);					
					ZipEntry e = new ZipEntry(zipEntry.getName());
					out.putNextEntry(e);
					byte[] data = xmlFile.getBytes();
					out.write(data, 0, data.length);
					out.closeEntry();					
				}
			}
		}		
	}
}
