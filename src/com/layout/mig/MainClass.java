package com.layout.mig;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.layout.mig.xml.sobject.ProcessSObjects;
import com.layout.mig.xml.ProcessLayouts;
import com.layout.mig.xml.ProcessProfiles;
import com.layut.mig.util.SFUtils;
import com.sforce.soap.metadata.RetrieveResult;

public class MainClass {

	public static void main(String[] args) throws Exception {		
		new MainClass().processMetadata();
	}
	
	public void processMetadata() throws Exception {
		SFUtils sfUtil = new SFUtils();
		RetrieveResult retrieveResult = sfUtil.retrieveInfo();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream zipOutStream = new ZipOutputStream(baos);
		new ProcessSObjects(retrieveResult, zipOutStream).process();
		new ProcessLayouts(retrieveResult, zipOutStream).process();
		new ProcessProfiles(retrieveResult, zipOutStream).process();
		addPackageXMLToZip(zipOutStream);
		sfUtil.deployInfo(baos.toByteArray());
		System.out.println("Config Page Layouts Setup...");
	}
	
	private void addPackageXMLToZip(ZipOutputStream zipOutStream) throws IOException {
		ZipEntry e = new ZipEntry("package.xml");
		zipOutStream.putNextEntry(e);       
        String strXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Package xmlns=\"http://soap.sforce.com/2006/04/metadata\"><types><members>Admin</members><name>Profile</name></types><types><members>*</members><name>Layout</name></types><types><members>*</members><name>CustomField</name></types><version>43.0</version></Package>";
        byte[] data = strXml.getBytes(); 
        zipOutStream.write(data, 0, data.length);
        zipOutStream.closeEntry();
        zipOutStream.close();
	}

}
